/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.converter.ConverterGenerico;
import br.com.comercial.entidade.Fabricante;
import br.com.comercial.facade.FabricanteFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author henrique
 */
@Named
@ViewScoped
public class FabricanteControle implements Serializable {

    private Fabricante fabricante;
    private List<Fabricante> listaFab;

    private Boolean layoutList = true;
    private Boolean layoutForm = false;

    @Inject
    private FabricanteFacade fabFacade;
    private ConverterGenerico converter;

    public ConverterGenerico getConverter() {
        if (converter == null) {
            converter = new ConverterGenerico(fabFacade);
        }
        return converter;
    }

    public void setFabricante(Fabricante fabricante) {
        layoutList = false;
        layoutForm = true;
        this.fabricante = fabricante;
    }

    public Fabricante getFabricante() {
        return fabricante;
    }

    public void salvar() {
        try {
            fabFacade.salvar(fabricante);
            layoutList = true;
            layoutForm = false;
            listaFab = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void excluir(Fabricante fabricante) {
        try {
            fabFacade.excluir(fabricante);
            listaFab = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the listFab
     */
    public List<Fabricante> getListaFab() {
        if (listaFab == null || listaFab.isEmpty()) {
            listaFab = fabFacade.listar();
        }
        return listaFab;
    }

    public Boolean getLayoutList() {
        return layoutList;
    }

    public Boolean getLayoutForm() {
        return layoutForm;
    }

    public void novo() {
        layoutForm = true;
        layoutList = false;
        fabricante = new Fabricante();
    }
}
