/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.Cliente;
import br.com.comercial.entidade.Comprador;
import br.com.comercial.facade.RelatorioFacade;
import br.com.comercial.util.ReportUtils;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JRException;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class RelatorioVeicControle implements Serializable {

    @Inject
    private RelatorioFacade relatorioFacade;
    private Date dataInicio;
    private Date dataFim;
    private Comprador cliente = new Comprador();
    @Inject
    private LoginControle loginControle;

    public void gerarRelatorioVendas() {
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ServletContext sc = (ServletContext) fc.getExternalContext().getContext();
            String caminhoReal = sc.getRealPath("/");

            Map<String, Object> parametros = new HashMap<>();

            parametros.put("SUBREPORT_DIR", caminhoReal + "WEB-INF/reports/");
            parametros.put("DATA_INICIO", dataInicio);
            parametros.put("DATA_FIM", dataFim);
            parametros.put("USUARIO", loginControle.getUsuario().getLogin());
            if (cliente != null && cliente.getId() != null) {
                parametros.put("CLIENTE", " and c.id=" + cliente.getId());
            } else {
                parametros.put("CLIENTE", "");
            }

            String relatorio = "WEB-INF/reports/relatorioVendaVeiculo.jasper";

            Connection conn = relatorioFacade.getConnection();

            ReportUtils reportUtils = new ReportUtils();

            reportUtils.gerarRelatorioPDF(parametros, relatorio, conn);

        } catch (SQLException | IOException | JRException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Comprador getCliente() {
        return cliente;
    }

    public void setCliente(Comprador cliente) {
        this.cliente = cliente;
    }

}
