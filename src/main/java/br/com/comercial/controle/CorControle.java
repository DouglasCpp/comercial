/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.converter.ConverterGenerico;
import br.com.comercial.entidade.Cor;
import br.com.comercial.facade.CorFacade;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author henrique
 */
@Named
@ViewScoped
public class CorControle implements Serializable {

    private Boolean layoutList = true;
    private Boolean layoutForm = false;
    private List<Cor> listaCor;
    @Inject
    private CorFacade corFacade;

    private Cor cor;

    private ConverterGenerico converter;
    
    public ConverterGenerico getConverter() {
        if(converter == null) {
            converter = new ConverterGenerico(corFacade);
        }
        return converter;
    }
    
    public void novo() {
        layoutList = false;
        layoutForm = true;
        cor = new Cor();
    }

    public void salvar() {
        try {
            corFacade.salvar(cor);
            layoutForm = false;
            layoutList = true;
            listaCor = null;
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Erro ao Salvar", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void carregarCor(Cor corParam) {
        layoutList = false;
        layoutForm = true;
        this.cor = corParam;
    }

    public void excluir(Cor cor) {
        try {
            corFacade.excluir(cor);
            listaCor = null;
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Erro ao exlucir", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public List<Cor> getListaCor() {
        if (listaCor == null || listaCor.isEmpty()) {
            listaCor = corFacade.listar();
        }
        return listaCor;
    }

    public Boolean getLayoutList() {
        return layoutList;
    }

    public Boolean getLayoutForm() {
        return layoutForm;
    }

    public Cor getCor() {
        return cor;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }

}
