/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.Fornecedor;
import br.com.comercial.entidade.PessoaJuridica;
import br.com.comercial.facade.AbstractFacade;
import br.com.comercial.facade.FornecedorFacade;
import br.com.comercial.facade.PessoaFacade;
import br.com.comercial.util.ControleUtil;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class FornecedorControle extends AbstractControle<Fornecedor>
        implements Serializable {

    @Inject
    private FornecedorFacade fornecedorFacade;
    @Inject
    private PessoaFacade pessoaFacade;
    private String cnpjConsulta;

    public FornecedorControle() {
        super(Fornecedor.class);
    }

    @Override
    public AbstractFacade getFacade() {
        return fornecedorFacade;
    }

    @Override
    public void alterar() {
        try {
        cnpjConsulta = super.getEntidade().getPessoaJuridica().getCnpj();
        super.alterar();
        } catch(NullPointerException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Selecione um fornecedor", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        
    }

    public void consultaCnpj() {
        try {
            PessoaJuridica pj = pessoaFacade.buscaPorCnpj(cnpjConsulta);
            super.getEntidade().setPessoaJuridica(pj);
        } catch (Exception ex) {
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_ERROR, "Pessoa Juridica não encontrada", ex.getMessage());
            ex.printStackTrace();
        }
        super.getEntidade().getPessoaJuridica().setCnpj(cnpjConsulta);
    }

    public String getCnpjConsulta() {
        return cnpjConsulta;
    }

    public void setCnpjConsulta(String cnpjConsulta) {
        this.cnpjConsulta = cnpjConsulta;
    }

}
