/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.Cliente;
import br.com.comercial.entidade.PessoaFisica;
import br.com.comercial.entidade.PessoaJuridica;
import br.com.comercial.facade.AbstractFacade;
import br.com.comercial.facade.ClienteFacade;
import br.com.comercial.facade.PessoaFacade;
import br.com.comercial.util.ControleUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class ClienteControle extends AbstractControle<Cliente> implements Serializable {

    @Inject
    private ClienteFacade clienteFacade;
    private String tipoPessoa = "PF";
    private String documentoFederal;
    @Inject
    private PessoaFacade pessoaFacade;

    @Override
    public void novo() {
        super.setEntidade(new Cliente());
        selecionaPessoa();
        super.setLayoutList(false);
        super.setLayoutForm(true);
    }

    @Override
    public void alterar() {
        try {
            if (super.getEntidade().getPessoa() instanceof PessoaJuridica) {
                tipoPessoa = "PJ";
            } else {
                tipoPessoa = "PF";
            }
            documentoFederal = getEntidade().getPessoa().getDocumentoFederal();
            super.alterar();
        } catch (NullPointerException e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Selecione um cliente", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void selecionaPessoa() {
        documentoFederal = "";
        if (tipoPessoa.equals("PF")) {
            super.getEntidade().setPessoa(new PessoaFisica());
        } else {
            super.getEntidade().setPessoa(new PessoaJuridica());
        }
    }

    public void consultaDocumentoFederal() {
        try {
            if (tipoPessoa.equals("PF")) {
                PessoaFisica pf = pessoaFacade.buscaPorCpf(documentoFederal);
                super.getEntidade().setPessoa(pf);
            } else {
                PessoaJuridica pj = pessoaFacade.buscaPorCnpj(documentoFederal);
                super.getEntidade().setPessoa(pj);
            }
        } catch (Exception ex) {
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_ERROR, "Pessoa não encontrada", ex.getMessage());
            ex.printStackTrace();
        }
        super.getEntidade().getPessoa().setDocumentoFederal(documentoFederal);
    }

    public List<Cliente> clienteLikeNome(String query) {
        return clienteFacade.clienteLikeNome(query);
    }

    public ClienteControle() {
        super(Cliente.class);
    }

    @Override
    public AbstractFacade<Cliente> getFacade() {
        return clienteFacade;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getDocumentoFederal() {
        return documentoFederal;
    }

    public void setDocumentoFederal(String documentoFederal) {
        this.documentoFederal = documentoFederal;
    }

}
