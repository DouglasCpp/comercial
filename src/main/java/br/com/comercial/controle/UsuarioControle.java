/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.Usuario;
import br.com.comercial.facade.AbstractFacade;
import br.com.comercial.facade.UsuarioFacade;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class UsuarioControle extends AbstractControle<Usuario>
        implements Serializable {

    @Inject
    private UsuarioFacade usuarioFacade;

    public UsuarioControle() {
        super(Usuario.class);
    }

    @Override
    public AbstractFacade getFacade() {
        return usuarioFacade;
    }

}
