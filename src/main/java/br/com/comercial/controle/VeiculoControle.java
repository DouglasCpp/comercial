/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.converter.ConverterGenerico;
import br.com.comercial.entidade.Veiculo;
import br.com.comercial.facade.VeiculoFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author henrique
 */

@Named
@ViewScoped
public class VeiculoControle implements Serializable {
    
    private Veiculo veiculo;
    private List<Veiculo> listaVeiculo;
    
    @Inject
    private VeiculoFacade veicFacade;
    
    private Boolean layoutForm = false;
    private Boolean layoutList = true;
    
    private ConverterGenerico converter;

    public ConverterGenerico getConverter() {
        if (converter == null) {
            converter = new ConverterGenerico(veicFacade);
        }
        return converter;
    }
    
    public Boolean getLayoutForm(){
        return layoutForm;
    }
    
    public Boolean getLayoutList(){
        return layoutList;
    }
    
    public Veiculo getVeiculo() {
        return veiculo;
    }
    
    public void setVeiculo(Veiculo veiculo) {
        layoutList = false;
        layoutForm = true;
        this.veiculo = veiculo;
    }
    
    public void salvar() {
        try {
            veicFacade.salvar(veiculo);
            layoutList = true;
            layoutForm = false;
            listaVeiculo = null;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<Veiculo> getListaVeiculo() {
        if(listaVeiculo == null || listaVeiculo.isEmpty()) {
            listaVeiculo = veicFacade.listar();
        }
        return listaVeiculo;
    }
    
    public void excluir(Veiculo veiculo) {
        try {
            veicFacade.excluir(veiculo);
            listaVeiculo = null;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void novo() {
        layoutList = false;
        layoutForm = true;
        veiculo = new Veiculo();
    }
    
    
}
