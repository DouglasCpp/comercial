/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.ItemVenda;
import br.com.comercial.entidade.VendaVeiculo;
import br.com.comercial.facade.VendaVeiculoFacade;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author henrique
 */
@Named
@ViewScoped
public class VendaVeiculoControle implements Serializable {

    private Boolean layoutList = true;
    private Boolean layoutForm = false;

    private VendaVeiculo vendaVeic;
    private List<VendaVeiculo> listaVeic;

    private ItemVenda itemVenda = new ItemVenda();

    @Inject
    private VendaVeiculoFacade facadeVeic;

    public Boolean getLayoutList() {
        return layoutList;
    }

    public Boolean getLayoutForm() {
        return layoutForm;
    }

    public VendaVeiculo getVendaVeic() {
        return vendaVeic;
    }

    public void setVendaVeic(VendaVeiculo vendaVeic) {
        this.vendaVeic = vendaVeic;
        facadeVeic.carregaItens(this.vendaVeic);
        layoutList = false;
        layoutForm = true;

    }

    public List<VendaVeiculo> getListaVeic() {
        if (listaVeic == null || listaVeic.isEmpty()) {
            listaVeic = facadeVeic.listar();
        }
        return listaVeic;
    }

    public void novo() {
        layoutForm = true;
        layoutList = false;
        vendaVeic = new VendaVeiculo();
        facadeVeic.carregaItens(this.vendaVeic);
        itemVenda = new ItemVenda();
    }

    public void salvar() {
        try {
            facadeVeic.salvar(vendaVeic);
            layoutForm = false;
            layoutList = true;
            listaVeic = null;
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Erro ao salvar", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void excluir(VendaVeiculo vendaVeic) {
        try {
            facadeVeic.carregaItens(vendaVeic);
            facadeVeic.excluir(vendaVeic);
            listaVeic = null;
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Erro ao excluir", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void adicionarItem() {

        getItemVenda().setVenda(vendaVeic);
        if (!facadeVeic.temEstoque(getItemVenda())) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    String.format("Estoque de %d %ss é insuficiente. Solicitados para venda: %d.",
                            getItemVenda().getVeiculo().getEstoque(), getItemVenda().getVeiculo().getNome(),
                            getItemVenda().getQuantidade()), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            getItemVenda().setValorUnitario(getItemVenda().getVeiculo().getPreco());
            getItemVenda().setBaixaEstoque(false);
            BigDecimal custo = getItemVenda().getValorUnitario();
            custo = custo.multiply(new BigDecimal(getItemVenda().getQuantidade()));
            vendaVeic.setValorTotal(vendaVeic.getValorTotal().add(custo));
            vendaVeic.setData(new Date());
            vendaVeic.setValorDesconto(BigDecimal.ZERO);
            vendaVeic.getItemVendas().add(getItemVenda());
            itemVenda = new ItemVenda();
        }
    }

    public void removerItem(ItemVenda itemVenda) {
        vendaVeic.getItemVendas().remove(itemVenda);
        BigDecimal custo = itemVenda.getValorUnitario();
        custo = custo.multiply(new BigDecimal(itemVenda.getQuantidade()));
        vendaVeic.setValorTotal(vendaVeic.getValorTotal().subtract(custo));
    }

    public void setItemVenda(ItemVenda itemVenda) {
        this.itemVenda = itemVenda;
    }

    public ItemVenda getItemVenda() {
        return itemVenda;
    }
}
