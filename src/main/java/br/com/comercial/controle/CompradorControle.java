/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.converter.ConverterGenerico;
import br.com.comercial.entidade.Comprador;
import br.com.comercial.facade.CompradorFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author henrique
 */
@Named
@ViewScoped
public class CompradorControle implements Serializable {

    private Comprador comprador;
    private List<Comprador> listaComprador;

    @Inject
    private CompradorFacade compradorFacade;
    
    private Boolean layoutList = true;
    private Boolean layoutForm = false;
    
    private ConverterGenerico converter;
    
    public ConverterGenerico getConverter() {
        if (converter == null) {
            converter = new ConverterGenerico(compradorFacade);
        }
        return converter;
    }
    
    public void setLayout() {
        layoutList = layoutList ? false: true;
        layoutForm = layoutForm ? false: true;
    }
    
    public void salvar() {
        try {
            setLayout();
            compradorFacade.salvar(comprador);
            listaComprador = null;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void excluir(Comprador comprador) {
        try {
            compradorFacade.excluir(comprador);
            listaComprador = null;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void novo() {
        comprador = new Comprador();
        setLayout();
    }

    public Boolean getLayoutList() {
        return layoutList;
    }
    
    public Boolean getLayoutForm() {
        return layoutForm;
    }

    public Comprador getComprador() {
        return comprador;
    }

    public void setComprador(Comprador comprador) {
        setLayout();
        this.comprador = comprador;
    }

    public List<Comprador> getListaComprador() {
        if(listaComprador == null || listaComprador.isEmpty()) {
            listaComprador = compradorFacade.listar();
        }
        return listaComprador;
    }
    
}
