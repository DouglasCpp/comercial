/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.ContaReceber;
import br.com.comercial.entidade.PessoaFisica;
import br.com.comercial.entidade.Venda;
import br.com.comercial.facade.ContaReceberFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author henrique
 */
@Named
@ViewScoped
public class ContaReceberControle implements Serializable {

    private Boolean layoutList = true;
    private Boolean layoutForm = false;

    @Inject
    private ContaReceberFacade facadeConta;
    
    private ContaReceber contaReceber;
    private List<ContaReceber> listContaReceber;

    public void setLayout() {
        layoutList = !layoutList;
        layoutForm = !layoutForm;
    }

    public Boolean getLayoutList() {
        return layoutList;
    }

    public Boolean getLayoutForm() {
        return layoutForm;
    }

    public ContaReceber getContaReceber() {
        return contaReceber;
    }

    public void setContaReceber(ContaReceber contaReceber) {
        this.contaReceber = contaReceber;
        setLayout();
    }

    public void novo() {
        contaReceber = new ContaReceber();
        contaReceber.setVenda(new Venda());
        contaReceber.setPessoa(new PessoaFisica());
        setLayout();
    }

    public void salvar() {
        try {            
            contaReceber.setVenda(null);
            facadeConta.salvar(contaReceber);
            listContaReceber = null;
            setLayout();
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar. ", e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void excluir(ContaReceber contaReceber) {
        try {
            facadeConta.excluir(contaReceber);
            listContaReceber = null;
        } catch (Exception e) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao excluir", e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public List<ContaReceber> getListContaReceber() {
        if (listContaReceber == null || listContaReceber.isEmpty()) {
            listContaReceber = facadeConta.listar();
        }
        return listContaReceber;
    }

    public void setListContaReceber(List<ContaReceber> listContaReceber) {
        this.listContaReceber = listContaReceber;
    }

}
