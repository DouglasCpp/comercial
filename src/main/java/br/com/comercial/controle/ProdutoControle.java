/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.Produto;
import br.com.comercial.facade.AbstractFacade;
import br.com.comercial.facade.ProdutoFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class ProdutoControle extends AbstractControle<Produto> 
                             implements Serializable{

    @Inject
    private ProdutoFacade produtoFacade;
    
    public ProdutoControle() {
        super(Produto.class);
    }

    @Override
    public AbstractFacade getFacade() {
        return produtoFacade;
    }
    
    public List<Produto> produtoLikeNome(String query){
        return produtoFacade.produtoLikeNome(query);
    }
  
}
