/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

/**
 *
 * @author ricardo
 */
public class QueryRelatorio {

    private final String query;

    public QueryRelatorio(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "<queryString><![CDATA["+query+"]]></queryString>";
    }

}
