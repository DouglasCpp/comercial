/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

import java.util.Date;

/**
 *
 * @author ricardo
 */
public class PageHeaderRelatorio {

    private final Date DataHora;
    private final String usuario;

    public PageHeaderRelatorio(String usuario) {
        this.usuario = usuario;
        this.DataHora = new Date();
    }

    public Date getDataHora() {
        return DataHora;
    }

    public String getUsuario() {
        return usuario;
    }

}
