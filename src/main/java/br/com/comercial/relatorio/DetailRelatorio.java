/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ricardo
 */
public class DetailRelatorio {

    private final String tagInicio = "<detail>"
            + "<band height=\"125\" splitType=\"Stretch\">";
    private List<CampoDetailRelatorio> campoDetailRelatorio = new ArrayList<>();
    private final String tagFim = "</band>"
            + "</detail>";

    public List<CampoDetailRelatorio> getCampoDetailRelatorio() {
        return campoDetailRelatorio;
    }

    @Override
    public String toString() {
        String retorno = tagInicio;
        for (CampoDetailRelatorio c : campoDetailRelatorio) {
            retorno += c.toString();
        }
        retorno += tagFim;
        return retorno;
    }

}
