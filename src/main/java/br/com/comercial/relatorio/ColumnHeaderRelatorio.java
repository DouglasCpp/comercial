/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ricardo
 */
public class ColumnHeaderRelatorio {

    private final String tagInicio = "<columnHeader>"
            + "<band height=\"61\" splitType=\"Stretch\">";
    private final List<CampoColumnHeader> campoColumnHeader = new ArrayList<>();
    private final String tagFim = "</band>"
            + "</columnHeader>";

    public List<CampoColumnHeader> getCampoColumnHeader() {
        return campoColumnHeader;
    }

    @Override
    public String toString() {
        String retorno=tagInicio;
        for (CampoColumnHeader c : campoColumnHeader) {
            retorno+=c.toString();
        }
        retorno+=tagFim;
        return retorno;
    }
    
    

}
