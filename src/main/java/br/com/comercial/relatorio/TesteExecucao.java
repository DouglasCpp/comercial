/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

import br.com.comercial.annotations.RufisField;
import br.com.comercial.annotations.RufisReports;
import br.com.comercial.entidade.Produto;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;

/**
 *
 * @author ricardo
 */
public class TesteExecucao {

    public static void main(String[] args) {
        TesteExecucao teste = new TesteExecucao();

        teste.geraRelatorio(Produto.class, "Eu");

    }

    public void geraRelatorio(Class classe, String usuario) {
        if (classe.isAnnotationPresent(RufisReports.class)) {
            RufisReports rufisReports = (RufisReports) classe.getAnnotation(RufisReports.class);

            Relatorio relatorio = new Relatorio(rufisReports.consulta(), rufisReports.titulo(), usuario);

            for (Field declaredField : classe.getDeclaredFields()) {
                if (declaredField.isAnnotationPresent(RufisField.class)) {
                    RufisField r = declaredField.getAnnotation(RufisField.class);
                    
                    FieldsRelatorio fieldsRelatorio = new FieldsRelatorio(r);
                    CampoColumnHeader campoColumnHeader = new CampoColumnHeader(r);
                    CampoDetailRelatorio campoDetailRelatorio = new CampoDetailRelatorio(r);
                    
                    relatorio.getFieldsRelatorio().add(fieldsRelatorio);
                    relatorio.getColumnHeaderRelatorio().getCampoColumnHeader().add(campoColumnHeader);
                    relatorio.getDetailRelatorio().getCampoDetailRelatorio().add(campoDetailRelatorio);
                }

            }
            
            System.out.println(relatorio.toString());
        } else {
            System.err.println("Classe não habilitada para gerar relatorio");
        }
    }
}
