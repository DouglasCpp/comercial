/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.ContaReceber;
import br.com.comercial.entidade.Produto;
import br.com.comercial.entidade.Venda;
import br.com.comercial.entidade.VendaItem;
import br.com.comercial.persistencia.Transacional;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author ricardo
 */
public class VendaFacade extends AbstractFacade<Venda> implements Serializable {

    @Inject
    private EntityManager em;
    @Inject
    private ProdutoFacade produtoFacade;
    @Inject
    private ContaReceberFacade crf;

    public VendaFacade() {
        super(Venda.class);
    }

    @Override
    @Transacional
    public Venda salvar(Venda entidade) throws Exception {
        try {
            entidade = super.salvar(entidade);

            // ------------------ Salvar itens da venda ------------------
            for (VendaItem i : entidade.getVendaItens()) {
                Produto p = produtoFacade.pesquisarId(i.getProduto().getId());
                p.setEstoque(p.getEstoque().subtract(i.getQuantidade()));
                produtoFacade.salvar(p);
            }

            // -------------- Salvar parcelas como conta a receber --------
            BigDecimal quantParcelas = new BigDecimal(entidade.getQuantParcelas());
            BigDecimal parcela = entidade.getTotal()
                    .subtract(entidade.getDesconto())
                    .divide((quantParcelas), 2, RoundingMode.DOWN);
            
            // Compensa dízima adicionando diferença à primeira parcela
            BigDecimal totalParcelas = parcela.multiply(quantParcelas);
            BigDecimal primeiraParcela = entidade.getTotal().subtract(totalParcelas);
            primeiraParcela = primeiraParcela.add(parcela);

            // Salva as parcelas
            for (int i = 0; i < entidade.getQuantParcelas(); i++) {
                ContaReceber contaReceber = new ContaReceber();
                contaReceber.setDataLancamento(new Date());
                contaReceber.setDataPagamento(new Date());
                Date venc = DateUtils.addMonths(entidade.getDataPrimeiraParcela(), i);
                contaReceber.setDataVencimento(venc);
                contaReceber.setDesconto(BigDecimal.ZERO);
                contaReceber.setStatus("A");
                contaReceber.setValorOriginal(
                        (parcela != primeiraParcela && i == 0) ? primeiraParcela: parcela
                );
                contaReceber.setValorRecebido(BigDecimal.ZERO);
                contaReceber.setPessoa(entidade.getCliente().getPessoa());
                contaReceber.setVenda(entidade);
                crf.salvar(contaReceber);
            }

            return entidade;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public EntityManager getEm() {
        return em;
    }

    @Override
    @Transacional
    public void excluir(Venda venda) throws Exception {
        try {
            carregaItenVenda(venda);
            for(VendaItem vi : venda.getVendaItens()) {
                Produto p = vi.getProduto();
                BigDecimal estoque = p.getEstoque().add(vi.getQuantidade());
                p.setEstoque(estoque);
                produtoFacade.salvar(p);
            }
            super.excluir(venda);
        } catch (Exception e) {
            throw e;
        }
    }

    public void carregaItenVenda(Venda v) {
        v.setVendaItens(em.createQuery("FROM VendaItem AS iv WHERE iv.venda=" + v).getResultList());
    }

}
