/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.Fabricante;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author henrique
 */
public class FabricanteFacade extends AbstractFacade<Fabricante> implements Serializable {
    @Inject
    private EntityManager em;
    
    public FabricanteFacade() {
        super(Fabricante.class);
    }
    
    @Override
    public EntityManager getEm() {
        return em;
    }
    
    @Override
    public List<Fabricante> listar() {
        return em.createNativeQuery("SELECT f.* FROM fabricante AS f", Fabricante.class).getResultList();
    }
}
