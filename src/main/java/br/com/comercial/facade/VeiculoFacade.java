/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.Veiculo;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 */
public class VeiculoFacade extends AbstractFacade<Veiculo> implements Serializable{

    @Inject
    private EntityManager em;

    public VeiculoFacade() {
        super(Veiculo.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    @Override
    public List<Veiculo> listar() {
        return em.createNativeQuery("SELECT v.* FROM veiculo AS v ORDER BY v.veic_id", Veiculo.class).getResultList();
    }
    
}
