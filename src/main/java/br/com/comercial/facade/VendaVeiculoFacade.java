/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.ItemVenda;
import br.com.comercial.entidade.Veiculo;
import br.com.comercial.entidade.VendaVeiculo;
import br.com.comercial.persistencia.Transacional;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 */
public class VendaVeiculoFacade extends AbstractFacade<VendaVeiculo> implements Serializable {

    @Inject
    private EntityManager em;

    public VendaVeiculoFacade() {
        super(VendaVeiculo.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }

    @Override
    public List<VendaVeiculo> listar() {
        return em.createNativeQuery("SELECT ve.* FROM venda_veiculo AS ve", VendaVeiculo.class).getResultList();
    }

    public void carregaItens(VendaVeiculo vendaVeiculo) {
        List<ItemVenda> itens = em.createNativeQuery("SELECT i.* from item_venda as i where i.venda_venda_id=" + vendaVeiculo.getId(),
                ItemVenda.class).getResultList();

        vendaVeiculo.setItemVendas(itens);
    }

    @Override
    @Transacional
    public VendaVeiculo salvar(VendaVeiculo entidade) throws Exception {
        entidade = super.salvar(entidade);
        baixarEstoqueVeiculo(entidade);
        return entidade;
    }

    @Override
    @Transacional
    public void excluir(VendaVeiculo entidade) throws Exception {
        try {
            super.excluir(entidade);
            voltaEstoqueVeiculo(entidade);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Transacional
    public void baixarEstoqueVeiculo(VendaVeiculo vendaVeic) throws Exception {
        try {
            for (ItemVenda itemVenda : vendaVeic.getItemVendas()) {
                if (!itemVenda.getBaixaEstoque()) {
                    Veiculo v = itemVenda.getVeiculo();
                    Integer estoque = v.getEstoque();
                    Integer quant = itemVenda.getQuantidade();
                    if (!temEstoque(itemVenda)) {
                        throw new Exception(String.format("Estoque de %d %ss é insuficiente. Solicitados para venda: %d.",
                                estoque, v.getNome(), quant));
                    }
                    v.setEstoque(estoque - quant);
                    itemVenda.setBaixaEstoque(Boolean.TRUE);
                    em.merge(v);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Transacional
    public Boolean temEstoque(ItemVenda item) {
        Integer estoque = item.getVeiculo().getEstoque();
        Integer quant = item.getQuantidade();
        return estoque >= quant;
    }

    @Transacional
    public void voltaEstoqueVeiculo(VendaVeiculo vendaVeic) throws Exception {
        try {
            for (ItemVenda itemVenda : vendaVeic.getItemVendas()) {
                Veiculo v = itemVenda.getVeiculo();
                v.setEstoque(v.getEstoque() + itemVenda.getQuantidade());
                em.merge(v);
            }
        } catch (Exception e) {
            throw e;
        }
    }

}
