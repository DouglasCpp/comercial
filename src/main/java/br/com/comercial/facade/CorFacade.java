/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.Cor;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 */
public class CorFacade extends AbstractFacade<Cor> implements Serializable{

    @Inject
    private EntityManager em;
    
    public CorFacade() {
        super(Cor.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    @Override
    public List<Cor> listar() {
        return em.createNativeQuery("SELECT c.* FROM cor AS c", Cor.class).getResultList();
    }
    
}
