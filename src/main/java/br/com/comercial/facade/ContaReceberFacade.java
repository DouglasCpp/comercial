/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.ContaReceber;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author henrique
 */
public class ContaReceberFacade extends AbstractFacade<ContaReceber> implements Serializable {

    @Inject
    private EntityManager em;

    public ContaReceberFacade() {
        super(ContaReceber.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }

    @Override
    public List<ContaReceber> listar() {
        return em.createNativeQuery("SELECT c.* FROM conta_receber AS c", ContaReceber.class).getResultList();
    }
}
