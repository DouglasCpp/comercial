/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.Usuario;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author ricardo
 */
public class UsuarioFacade extends AbstractFacade<Usuario> implements Serializable {

    @Inject
    private EntityManager em;

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }

    public Usuario findUsuario(String login, String senha) throws Exception {
        try {
            Query q = em.createQuery("FROM Usuario AS u WHERE u.login='" + login + "' AND u.senha='" + senha + "'");
            Usuario u = (Usuario) q.getSingleResult();
            return u;
        } catch (Exception e) {
            throw new Exception("Usuário não encontrado, motivo: " + e.getMessage());
        }
    }

}
