/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.Cliente;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author ricardo
 */
public class ClienteFacade extends AbstractFacade<Cliente> implements Serializable{

    @Inject
    private EntityManager em;
    
    public ClienteFacade() {
        super(Cliente.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    public List<Cliente> clienteLikeNome(String query){
        Query q = em.createQuery("FROM Cliente AS c "
        + "WHERE LOWER(c.pessoa.nome) LIKE ('%"+query.toLowerCase()+"%')");
        return q.getResultList();
    }
    
}
