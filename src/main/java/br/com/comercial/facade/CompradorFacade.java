/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.Comprador;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

/**
 *
 * @author henrique
 */
public class CompradorFacade extends AbstractFacade<Comprador> implements Serializable {

    @Inject
    private EntityManager em;
    
    public CompradorFacade() {
        super(Comprador.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    @Override
    public List<Comprador> listar() {
        return em.createNativeQuery("SELECT co.* FROM comprador AS co", Comprador.class).getResultList();
    }
    
}
