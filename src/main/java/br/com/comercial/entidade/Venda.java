/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

/**
 *
 * @author ricardo
 */
@Entity
@Audited
@Table(name = "venda")
public class Venda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ven_id")
    private Long id;
    @Column(name = "ven_total", nullable = false)
    private BigDecimal total;
    @Column(name = "ven_desconto")
    private BigDecimal desconto = BigDecimal.ZERO;
    @Column(name = "ven_dtvenda")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dtvenda = new Date();
    @Column(name = "ven_qtparcelas")
    private Integer quantParcelas = 1;
    @Column(name = "dt_primeira_parcela") // Data da primeira parcela
    private Date dataPrimeiraParcela;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Cliente cliente;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "venda", orphanRemoval = true)
    private List<VendaItem> vendaItens = new ArrayList<>();
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "venda", orphanRemoval = true)
    private List<ContaReceber> contas = new ArrayList<>();

    @Transient
    private VendaItem vendaItem = new VendaItem();

    @Transient
    private BigDecimal descontoArredondamento = BigDecimal.ZERO;

    public void adicionaItem() {
        if (vendaItem.getQuantidade().compareTo(BigDecimal.ZERO) > 0) {
            vendaItem.setVenda(this);
            if (!vendaItens.contains(vendaItem)) {
                vendaItens.add(vendaItem);
            } else {
                VendaItem iv = vendaItens.get(vendaItens.indexOf(vendaItem));
                iv.setQuantidade(iv.getQuantidade().add(vendaItem.getQuantidade()));
                iv.setDesconto(iv.getDesconto().add(vendaItem.getDesconto()));
            }
            calculaTotal();
            vendaItem = new VendaItem();
        }
    }

    public void calculaTotal() {
        total = BigDecimal.ZERO;
        desconto = BigDecimal.ZERO;
        for (VendaItem vi : vendaItens) {
            total = vi.getPreco().multiply(vi.getQuantidade()).add(total);
        }
        if (descontoArredondamento.compareTo(BigDecimal.ZERO) > 0 && descontoArredondamento.compareTo(total) < 0) {

            BigDecimal descArredondaItem = BigDecimal.ZERO;

            if (vendaItens.size() > 0) {
                descArredondaItem = descontoArredondamento
                        .divide(new BigDecimal(vendaItens.size()), 2, RoundingMode.HALF_EVEN);
            } else {
                descontoArredondamento = BigDecimal.ZERO;
            }

            for (VendaItem vi : vendaItens) {
                vi.setDesArredondamento(descArredondaItem);
                desconto = vi.getDesconto().add(desconto);
            }
            total = total.subtract(getDesconto());
        }else{
            descontoArredondamento=BigDecimal.ZERO;
        }
    }

    public void removeItem(VendaItem item) {
        vendaItens.remove(item);
        calculaTotal();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getDesconto() {
        return desconto.add(descontoArredondamento);
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public Date getDtvenda() {
        return dtvenda;
    }

    public void setDtvenda(Date dtvenda) {
        this.dtvenda = dtvenda;
    }

    public List<VendaItem> getVendaItens() {
        return vendaItens;
    }

    public void setVendaItens(List<VendaItem> vendaItens) {
        this.vendaItens = vendaItens;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public VendaItem getVendaItem() {
        return vendaItem;
    }

    public void setVendaItem(VendaItem vendaItem) {
        this.vendaItem = vendaItem;
    }

    public BigDecimal getDescontoArredondamento() {
        return descontoArredondamento;
    }

    public void setDescontoArredondamento(BigDecimal descontoArredondamento) {
        this.descontoArredondamento = descontoArredondamento;
    }
    
    public Integer getQuantParcelas() {
        return quantParcelas;
    }

    public void setQuantParcelas(Integer quantParcelas) {
        this.quantParcelas = quantParcelas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Venda)) {
            return false;
        }
        Venda other = (Venda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id.toString();
    }

    public List<ContaReceber> getContas() {
        return contas;
    }

    public void setContas(List<ContaReceber> contas) {
        this.contas = contas;
    }

    public Date getDataPrimeiraParcela() {
        return dataPrimeiraParcela;
    }

    public void setDataPrimeiraParcela(Date dataPrimeiraParcela) {
        this.dataPrimeiraParcela = dataPrimeiraParcela;
    }

}
